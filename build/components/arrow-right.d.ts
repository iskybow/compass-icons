import React from 'react';
import IconProps from './props';
declare const ArrowRightIcon: React.FC<IconProps>;
export default ArrowRightIcon;

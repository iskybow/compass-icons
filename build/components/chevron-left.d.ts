import React from 'react';
import IconProps from './props';
declare const ChevronLeftIcon: React.FC<IconProps>;
export default ChevronLeftIcon;

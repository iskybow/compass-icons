import React from 'react';
import IconProps from './props';
declare const FileMultipleOutlineIcon: React.FC<IconProps>;
export default FileMultipleOutlineIcon;

import React from 'react';
import IconProps from './props';
declare const ForumOutlineIcon: React.FC<IconProps>;
export default ForumOutlineIcon;

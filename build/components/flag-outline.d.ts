import React from 'react';
import IconProps from './props';
declare const FlagOutlineIcon: React.FC<IconProps>;
export default FlagOutlineIcon;

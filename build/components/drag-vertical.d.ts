import React from 'react';
import IconProps from './props';
declare const DragVerticalIcon: React.FC<IconProps>;
export default DragVerticalIcon;

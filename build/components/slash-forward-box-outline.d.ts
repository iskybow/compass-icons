import React from 'react';
import IconProps from './props';
declare const SlashForwardBoxOutlineIcon: React.FC<IconProps>;
export default SlashForwardBoxOutlineIcon;

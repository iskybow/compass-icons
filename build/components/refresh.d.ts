import React from 'react';
import IconProps from './props';
declare const RefreshIcon: React.FC<IconProps>;
export default RefreshIcon;

import React from 'react';
import IconProps from './props';
declare const LinkVariantOffIcon: React.FC<IconProps>;
export default LinkVariantOffIcon;

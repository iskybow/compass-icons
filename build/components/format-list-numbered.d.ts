import React from 'react';
import IconProps from './props';
declare const FormatListNumberedIcon: React.FC<IconProps>;
export default FormatListNumberedIcon;

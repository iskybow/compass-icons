import React from 'react';
import IconProps from './props';
declare const MenuVariantIcon: React.FC<IconProps>;
export default MenuVariantIcon;

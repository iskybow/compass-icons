import React from 'react';
import IconProps from './props';
declare const EmoticonPlusOutlineIcon: React.FC<IconProps>;
export default EmoticonPlusOutlineIcon;

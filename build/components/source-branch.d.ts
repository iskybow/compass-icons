import React from 'react';
import IconProps from './props';
declare const SourceBranchIcon: React.FC<IconProps>;
export default SourceBranchIcon;

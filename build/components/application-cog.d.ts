import React from 'react';
import IconProps from './props';
declare const ApplicationCogIcon: React.FC<IconProps>;
export default ApplicationCogIcon;

import React from 'react';
import IconProps from './props';
declare const StarOutlineIcon: React.FC<IconProps>;
export default StarOutlineIcon;

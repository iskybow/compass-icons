import React from 'react';
import IconProps from './props';
declare const CheckCircleOutlineIcon: React.FC<IconProps>;
export default CheckCircleOutlineIcon;

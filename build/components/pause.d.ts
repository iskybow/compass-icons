import React from 'react';
import IconProps from './props';
declare const PauseIcon: React.FC<IconProps>;
export default PauseIcon;

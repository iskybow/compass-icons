import React from 'react';
import IconProps from './props';
declare const ArrowExpandAllIcon: React.FC<IconProps>;
export default ArrowExpandAllIcon;

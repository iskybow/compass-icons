import React from 'react';
import IconProps from './props';
declare const BellOutlineIcon: React.FC<IconProps>;
export default BellOutlineIcon;

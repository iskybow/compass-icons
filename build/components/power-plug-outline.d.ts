import React from 'react';
import IconProps from './props';
declare const PowerPlugOutlineIcon: React.FC<IconProps>;
export default PowerPlugOutlineIcon;

import React from 'react';
import IconProps from './props';
declare const LockOutlineIcon: React.FC<IconProps>;
export default LockOutlineIcon;

import React from 'react';
import IconProps from './props';
declare const ExitToAppIcon: React.FC<IconProps>;
export default ExitToAppIcon;

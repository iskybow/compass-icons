import React from 'react';
import IconProps from './props';
declare const CheckboxMarkedIcon: React.FC<IconProps>;
export default CheckboxMarkedIcon;

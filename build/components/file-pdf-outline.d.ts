import React from 'react';
import IconProps from './props';
declare const FilePdfOutlineIcon: React.FC<IconProps>;
export default FilePdfOutlineIcon;

import React from 'react';
import IconProps from './props';
declare const ChevronDownCircleOutlineIcon: React.FC<IconProps>;
export default ChevronDownCircleOutlineIcon;

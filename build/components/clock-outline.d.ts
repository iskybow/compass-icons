import React from 'react';
import IconProps from './props';
declare const ClockOutlineIcon: React.FC<IconProps>;
export default ClockOutlineIcon;

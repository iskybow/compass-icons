import React from 'react';
import IconProps from './props';
declare const CircleOutlineIcon: React.FC<IconProps>;
export default CircleOutlineIcon;

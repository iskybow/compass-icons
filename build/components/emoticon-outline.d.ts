import React from 'react';
import IconProps from './props';
declare const EmoticonOutlineIcon: React.FC<IconProps>;
export default EmoticonOutlineIcon;

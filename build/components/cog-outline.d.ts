import React from 'react';
import IconProps from './props';
declare const CogOutlineIcon: React.FC<IconProps>;
export default CogOutlineIcon;

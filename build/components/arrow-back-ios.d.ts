import React from 'react';
import IconProps from './props';
declare const ArrowBackIosIcon: React.FC<IconProps>;
export default ArrowBackIosIcon;

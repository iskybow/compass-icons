import React from 'react';
import IconProps from './props';
declare const EmailOutlineIcon: React.FC<IconProps>;
export default EmailOutlineIcon;

import React from 'react';
import IconProps from './props';
declare const ArrowUpBoldCircleOutlineIcon: React.FC<IconProps>;
export default ArrowUpBoldCircleOutlineIcon;

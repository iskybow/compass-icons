import React from 'react';
import IconProps from './props';
declare const CalendarMonthOutlineIcon: React.FC<IconProps>;
export default CalendarMonthOutlineIcon;

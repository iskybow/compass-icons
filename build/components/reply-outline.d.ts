import React from 'react';
import IconProps from './props';
declare const ReplyOutlineIcon: React.FC<IconProps>;
export default ReplyOutlineIcon;

import React from 'react';
import IconProps from './props';
declare const FormatHeaderIcon: React.FC<IconProps>;
export default FormatHeaderIcon;

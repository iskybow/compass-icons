import React from 'react';
import IconProps from './props';
declare const CarOutlineIcon: React.FC<IconProps>;
export default CarOutlineIcon;

import React from 'react';
import IconProps from './props';
declare const MessageCheckOutlineIcon: React.FC<IconProps>;
export default MessageCheckOutlineIcon;

import React from 'react';
import IconProps from './props';
declare const ChevronRightIcon: React.FC<IconProps>;
export default ChevronRightIcon;

import React from 'react';
import IconProps from './props';
declare const FileGenericOutlineLargeIcon: React.FC<IconProps>;
export default FileGenericOutlineLargeIcon;

import React from 'react';
import IconProps from './props';
declare const MagnifyPlusIcon: React.FC<IconProps>;
export default MagnifyPlusIcon;

import React from 'react';
import IconProps from './props';
declare const FileExcelOutlineIcon: React.FC<IconProps>;
export default FileExcelOutlineIcon;

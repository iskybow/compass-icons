import React from 'react';
import IconProps from './props';
declare const LinkVariantIcon: React.FC<IconProps>;
export default LinkVariantIcon;

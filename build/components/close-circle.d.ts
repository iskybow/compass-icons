import React from 'react';
import IconProps from './props';
declare const CloseCircleIcon: React.FC<IconProps>;
export default CloseCircleIcon;

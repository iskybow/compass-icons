import React from 'react';
import IconProps from './props';
declare const EyeOffOutlineIcon: React.FC<IconProps>;
export default EyeOffOutlineIcon;

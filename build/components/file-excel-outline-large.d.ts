import React from 'react';
import IconProps from './props';
declare const FileExcelOutlineLargeIcon: React.FC<IconProps>;
export default FileExcelOutlineLargeIcon;

import React from 'react';
import IconProps from './props';
declare const AccountMultipleOutlineIcon: React.FC<IconProps>;
export default AccountMultipleOutlineIcon;

import React from 'react';
import IconProps from './props';
declare const HomeVariantOutlineIcon: React.FC<IconProps>;
export default HomeVariantOutlineIcon;

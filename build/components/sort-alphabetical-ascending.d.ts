import React from 'react';
import IconProps from './props';
declare const SortAlphabeticalAscendingIcon: React.FC<IconProps>;
export default SortAlphabeticalAscendingIcon;

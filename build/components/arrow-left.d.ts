import React from 'react';
import IconProps from './props';
declare const ArrowLeftIcon: React.FC<IconProps>;
export default ArrowLeftIcon;

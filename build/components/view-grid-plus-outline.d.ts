import React from 'react';
import IconProps from './props';
declare const ViewGridPlusOutlineIcon: React.FC<IconProps>;
export default ViewGridPlusOutlineIcon;

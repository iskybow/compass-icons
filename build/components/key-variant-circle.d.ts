import React from 'react';
import IconProps from './props';
declare const KeyVariantCircleIcon: React.FC<IconProps>;
export default KeyVariantCircleIcon;

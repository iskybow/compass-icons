import React from 'react';
import IconProps from './props';
declare const GithubCircleIcon: React.FC<IconProps>;
export default GithubCircleIcon;

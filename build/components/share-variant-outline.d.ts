import React from 'react';
import IconProps from './props';
declare const ShareVariantOutlineIcon: React.FC<IconProps>;
export default ShareVariantOutlineIcon;

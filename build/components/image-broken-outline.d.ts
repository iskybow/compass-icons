import React from 'react';
import IconProps from './props';
declare const ImageBrokenOutlineIcon: React.FC<IconProps>;
export default ImageBrokenOutlineIcon;

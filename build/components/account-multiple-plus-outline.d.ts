import React from 'react';
import IconProps from './props';
declare const AccountMultiplePlusOutlineIcon: React.FC<IconProps>;
export default AccountMultiplePlusOutlineIcon;

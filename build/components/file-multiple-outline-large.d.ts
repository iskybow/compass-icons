import React from 'react';
import IconProps from './props';
declare const FileMultipleOutlineLargeIcon: React.FC<IconProps>;
export default FileMultipleOutlineLargeIcon;

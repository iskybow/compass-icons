import React from 'react';
import IconProps from './props';
declare const FormatListBulletedIcon: React.FC<IconProps>;
export default FormatListBulletedIcon;

import React from 'react';
import IconProps from './props';
declare const FilePatchOutlineIcon: React.FC<IconProps>;
export default FilePatchOutlineIcon;

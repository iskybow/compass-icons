import React from 'react';
import IconProps from './props';
declare const FileGenericOutlineIcon: React.FC<IconProps>;
export default FileGenericOutlineIcon;

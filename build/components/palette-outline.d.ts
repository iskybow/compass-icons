import React from 'react';
import IconProps from './props';
declare const PaletteOutlineIcon: React.FC<IconProps>;
export default PaletteOutlineIcon;

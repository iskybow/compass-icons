import React from 'react';
import IconProps from './props';
declare const NotebookOutlineIcon: React.FC<IconProps>;
export default NotebookOutlineIcon;

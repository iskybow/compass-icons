import React from 'react';
import IconProps from './props';
declare const FormatBoldIcon: React.FC<IconProps>;
export default FormatBoldIcon;

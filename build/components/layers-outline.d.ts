import React from 'react';
import IconProps from './props';
declare const LayersOutlineIcon: React.FC<IconProps>;
export default LayersOutlineIcon;

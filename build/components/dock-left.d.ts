import React from 'react';
import IconProps from './props';
declare const DockLeftIcon: React.FC<IconProps>;
export default DockLeftIcon;

import React from 'react';
import IconProps from './props';
declare const PlaylistCheckIcon: React.FC<IconProps>;
export default PlaylistCheckIcon;

import React from 'react';
import IconProps from './props';
declare const FormatLetterCaseIcon: React.FC<IconProps>;
export default FormatLetterCaseIcon;

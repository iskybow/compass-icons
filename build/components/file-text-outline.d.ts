import React from 'react';
import IconProps from './props';
declare const FileTextOutlineIcon: React.FC<IconProps>;
export default FileTextOutlineIcon;

import React from 'react';
import IconProps from './props';
declare const ArchiveOutlineIcon: React.FC<IconProps>;
export default ArchiveOutlineIcon;

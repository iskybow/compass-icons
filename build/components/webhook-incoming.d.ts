import React from 'react';
import IconProps from './props';
declare const WebhookIncomingIcon: React.FC<IconProps>;
export default WebhookIncomingIcon;

import React from 'react';
import IconProps from './props';
declare const ImageOutlineIcon: React.FC<IconProps>;
export default ImageOutlineIcon;

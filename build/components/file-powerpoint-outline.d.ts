import React from 'react';
import IconProps from './props';
declare const FilePowerpointOutlineIcon: React.FC<IconProps>;
export default FilePowerpointOutlineIcon;

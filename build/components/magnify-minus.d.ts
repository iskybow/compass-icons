import React from 'react';
import IconProps from './props';
declare const MagnifyMinusIcon: React.FC<IconProps>;
export default MagnifyMinusIcon;

import React from 'react';
import IconProps from './props';
declare const TrashCanOutlineIcon: React.FC<IconProps>;
export default TrashCanOutlineIcon;

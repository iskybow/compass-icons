import React from 'react';
import IconProps from './props';
declare const CloseIcon: React.FC<IconProps>;
export default CloseIcon;

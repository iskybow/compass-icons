import React from 'react';
import IconProps from './props';
declare const FileWordOutlineIcon: React.FC<IconProps>;
export default FileWordOutlineIcon;

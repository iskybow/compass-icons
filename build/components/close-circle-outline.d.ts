import React from 'react';
import IconProps from './props';
declare const CloseCircleOutlineIcon: React.FC<IconProps>;
export default CloseCircleOutlineIcon;

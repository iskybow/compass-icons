import React from 'react';
import IconProps from './props';
declare const FileImageBrokenOutlineIcon: React.FC<IconProps>;
export default FileImageBrokenOutlineIcon;

import React from 'react';
import IconProps from './props';
declare const DockWindowIcon: React.FC<IconProps>;
export default DockWindowIcon;

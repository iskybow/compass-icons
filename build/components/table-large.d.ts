import React from 'react';
import IconProps from './props';
declare const TableLargeIcon: React.FC<IconProps>;
export default TableLargeIcon;

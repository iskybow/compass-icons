import React from 'react';
import IconProps from './props';
declare const CalendarCheckOutlineIcon: React.FC<IconProps>;
export default CalendarCheckOutlineIcon;

import React from 'react';
import IconProps from './props';
declare const ImageAreaOutlineIcon: React.FC<IconProps>;
export default ImageAreaOutlineIcon;

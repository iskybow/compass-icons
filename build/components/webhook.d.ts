import React from 'react';
import IconProps from './props';
declare const WebhookIcon: React.FC<IconProps>;
export default WebhookIcon;

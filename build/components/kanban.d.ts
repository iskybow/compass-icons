import React from 'react';
import IconProps from './props';
declare const KanbanIcon: React.FC<IconProps>;
export default KanbanIcon;

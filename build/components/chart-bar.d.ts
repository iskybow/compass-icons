import React from 'react';
import IconProps from './props';
declare const ChartBarIcon: React.FC<IconProps>;
export default ChartBarIcon;

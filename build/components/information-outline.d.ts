import React from 'react';
import IconProps from './props';
declare const InformationOutlineIcon: React.FC<IconProps>;
export default InformationOutlineIcon;

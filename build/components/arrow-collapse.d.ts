import React from 'react';
import IconProps from './props';
declare const ArrowCollapseIcon: React.FC<IconProps>;
export default ArrowCollapseIcon;

import React from 'react';
import IconProps from './props';
declare const FileTextOutlineLargeIcon: React.FC<IconProps>;
export default FileTextOutlineLargeIcon;

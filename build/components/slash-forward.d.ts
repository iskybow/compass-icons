import React from 'react';
import IconProps from './props';
declare const SlashForwardIcon: React.FC<IconProps>;
export default SlashForwardIcon;

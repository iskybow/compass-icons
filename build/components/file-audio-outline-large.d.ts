import React from 'react';
import IconProps from './props';
declare const FileAudioOutlineLargeIcon: React.FC<IconProps>;
export default FileAudioOutlineLargeIcon;

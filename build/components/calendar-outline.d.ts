import React from 'react';
import IconProps from './props';
declare const CalendarOutlineIcon: React.FC<IconProps>;
export default CalendarOutlineIcon;

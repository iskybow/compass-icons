import React from 'react';
import IconProps from './props';
declare const AlertCircleOutlineIcon: React.FC<IconProps>;
export default AlertCircleOutlineIcon;

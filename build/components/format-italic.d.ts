import React from 'react';
import IconProps from './props';
declare const FormatItalicIcon: React.FC<IconProps>;
export default FormatItalicIcon;

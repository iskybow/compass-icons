import React from 'react';
import IconProps from './props';
declare const FolderPlusOutlineIcon: React.FC<IconProps>;
export default FolderPlusOutlineIcon;

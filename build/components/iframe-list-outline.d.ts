import React from 'react';
import IconProps from './props';
declare const IframeListOutlineIcon: React.FC<IconProps>;
export default IframeListOutlineIcon;

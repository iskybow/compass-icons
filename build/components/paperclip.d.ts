import React from 'react';
import IconProps from './props';
declare const PaperclipIcon: React.FC<IconProps>;
export default PaperclipIcon;

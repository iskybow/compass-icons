"use strict";
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(require("react"));
const FileImageBrokenOutlineLargeIcon = (_a) => {
    var { size, color } = _a, rest = __rest(_a, ["size", "color"]);
    return (react_1.default.createElement("svg", Object.assign({ xmlns: "http://www.w3.org/2000/svg", version: "1.1", width: size || 24, height: size || 24, fill: color || '#000000', viewBox: "0 0 24 24" }, rest),
        react_1.default.createElement("path", { d: "M16.889,14.122c-0.094-0.096-0.222-0.149-0.355-0.15c0,0-0.001,0-0.002,0c-0.133,0-0.26,0.053-0.354,0.146l-2.711,2.712\nl-2.674-2.674c-0.195-0.195-0.512-0.195-0.707,0l-2.611,2.611l-2.621-2.621c-0.143-0.143-0.358-0.185-0.545-0.108\n\tC4.122,14.115,4,14.298,4,14.5v0.791v1.224V20c0,1.103,0.897,2,2,2h12c1.103,0,2-0.897,2-2v-2.5c0-0.131-0.052-0.257-0.143-0.35\n\tL16.889,14.122z M19,20c0,0.551-0.449,1-1,1H6c-0.552,0-1-0.449-1-1v-4.293l2.121,2.121c0.195,0.195,0.512,0.195,0.707,0\n\tl2.611-2.611l2.674,2.674c0.188,0.188,0.52,0.188,0.707,0l2.707-2.708L19,17.704V20z M18,2H9.949C9.552,2,9.178,2.154,8.895,2.432\n\tl-4.449,4.39C4.163,7.103,4,7.492,4,7.89v2.61c0,0.133,0.053,0.26,0.146,0.354l2.975,2.975c0.195,0.195,0.512,0.195,0.707,0\n\tl2.611-2.611l2.674,2.674c0.094,0.094,0.221,0.146,0.354,0.146s0.26-0.053,0.354-0.146l2.707-2.708l2.615,2.667\n\tc0.143,0.146,0.356,0.19,0.547,0.113C19.877,13.886,20,13.703,20,13.5v-0.697V4C20,2.897,19.103,2,18,2z M9,3.734V7.5H5.184L9,3.734\n\tz M19,12.276l-2.111-2.154c-0.094-0.095-0.222-0.149-0.355-0.15c0,0-0.001,0-0.002,0c-0.133,0-0.26,0.053-0.354,0.146l-2.711,2.712\n\tl-2.674-2.674c-0.195-0.195-0.512-0.195-0.707,0l-2.611,2.611L5,10.293V9.329V8.5h4c0.552,0,1-0.448,1-1V3h8c0.551,0,1,0.448,1,1\n\tV12.276z" })));
};
exports.default = FileImageBrokenOutlineLargeIcon;

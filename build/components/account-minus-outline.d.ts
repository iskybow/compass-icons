import React from 'react';
import IconProps from './props';
declare const AccountMinusOutlineIcon: React.FC<IconProps>;
export default AccountMinusOutlineIcon;

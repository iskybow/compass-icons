import React from 'react';
import IconProps from './props';
declare const AirplaneVariantIcon: React.FC<IconProps>;
export default AirplaneVariantIcon;

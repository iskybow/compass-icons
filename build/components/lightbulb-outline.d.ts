import React from 'react';
import IconProps from './props';
declare const LightbulbOutlineIcon: React.FC<IconProps>;
export default LightbulbOutlineIcon;

import React from 'react';
import IconProps from './props';
declare const VideoOutlineIcon: React.FC<IconProps>;
export default VideoOutlineIcon;

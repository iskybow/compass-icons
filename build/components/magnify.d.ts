import React from 'react';
import IconProps from './props';
declare const MagnifyIcon: React.FC<IconProps>;
export default MagnifyIcon;

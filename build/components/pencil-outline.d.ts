import React from 'react';
import IconProps from './props';
declare const PencilOutlineIcon: React.FC<IconProps>;
export default PencilOutlineIcon;

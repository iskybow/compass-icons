import React from 'react';
import IconProps from './props';
declare const BookmarkOutlineIcon: React.FC<IconProps>;
export default BookmarkOutlineIcon;

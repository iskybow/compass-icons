import React from 'react';
import IconProps from './props';
declare const EmoticonCustomOutlineIcon: React.FC<IconProps>;
export default EmoticonCustomOutlineIcon;

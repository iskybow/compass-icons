import React from 'react';
import IconProps from './props';
declare const AccountPlusOutlineIcon: React.FC<IconProps>;
export default AccountPlusOutlineIcon;

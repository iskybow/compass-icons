import React from 'react';
import IconProps from './props';
declare const ShieldAlertOutlineIcon: React.FC<IconProps>;
export default ShieldAlertOutlineIcon;

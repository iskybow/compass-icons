import React from 'react';
import IconProps from './props';
declare const PinOutlineIcon: React.FC<IconProps>;
export default PinOutlineIcon;

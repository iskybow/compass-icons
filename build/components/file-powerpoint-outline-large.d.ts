import React from 'react';
import IconProps from './props';
declare const FilePowerpointOutlineLargeIcon: React.FC<IconProps>;
export default FilePowerpointOutlineLargeIcon;

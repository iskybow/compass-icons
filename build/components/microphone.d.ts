import React from 'react';
import IconProps from './props';
declare const MicrophoneIcon: React.FC<IconProps>;
export default MicrophoneIcon;

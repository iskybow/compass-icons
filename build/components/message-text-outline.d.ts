import React from 'react';
import IconProps from './props';
declare const MessageTextOutlineIcon: React.FC<IconProps>;
export default MessageTextOutlineIcon;

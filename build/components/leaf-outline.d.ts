import React from 'react';
import IconProps from './props';
declare const LeafOutlineIcon: React.FC<IconProps>;
export default LeafOutlineIcon;

import React from 'react';
import IconProps from './props';
declare const MinusCircleIcon: React.FC<IconProps>;
export default MinusCircleIcon;

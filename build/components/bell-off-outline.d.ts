import React from 'react';
import IconProps from './props';
declare const BellOffOutlineIcon: React.FC<IconProps>;
export default BellOffOutlineIcon;

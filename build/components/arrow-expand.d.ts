import React from 'react';
import IconProps from './props';
declare const ArrowExpandIcon: React.FC<IconProps>;
export default ArrowExpandIcon;

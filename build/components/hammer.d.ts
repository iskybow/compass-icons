import React from 'react';
import IconProps from './props';
declare const HammerIcon: React.FC<IconProps>;
export default HammerIcon;

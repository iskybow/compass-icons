import React from 'react';
import IconProps from './props';
declare const RadioboxMarkedIcon: React.FC<IconProps>;
export default RadioboxMarkedIcon;

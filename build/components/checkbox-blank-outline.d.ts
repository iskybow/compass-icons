import React from 'react';
import IconProps from './props';
declare const CheckboxBlankOutlineIcon: React.FC<IconProps>;
export default CheckboxBlankOutlineIcon;

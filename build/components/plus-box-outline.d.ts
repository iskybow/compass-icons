import React from 'react';
import IconProps from './props';
declare const PlusBoxOutlineIcon: React.FC<IconProps>;
export default PlusBoxOutlineIcon;

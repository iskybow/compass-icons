import React from 'react';
import IconProps from './props';
declare const DotsVerticalIcon: React.FC<IconProps>;
export default DotsVerticalIcon;

import React from 'react';
import IconProps from './props';
declare const EmailPlusOutlineIcon: React.FC<IconProps>;
export default EmailPlusOutlineIcon;

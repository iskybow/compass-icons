import React from 'react';
import IconProps from './props';
declare const FlaskOutlineIcon: React.FC<IconProps>;
export default FlaskOutlineIcon;

import React from 'react';
import IconProps from './props';
declare const MarkAsUnreadIcon: React.FC<IconProps>;
export default MarkAsUnreadIcon;

import React from 'react';
import IconProps from './props';
declare const OpenInNewIcon: React.FC<IconProps>;
export default OpenInNewIcon;

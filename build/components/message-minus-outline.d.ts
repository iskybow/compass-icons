import React from 'react';
import IconProps from './props';
declare const MessageMinusOutlineIcon: React.FC<IconProps>;
export default MessageMinusOutlineIcon;

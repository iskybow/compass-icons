import React from 'react';
import IconProps from './props';
declare const InfinityIcon: React.FC<IconProps>;
export default InfinityIcon;

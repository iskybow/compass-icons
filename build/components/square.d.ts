import React from 'react';
import IconProps from './props';
declare const SquareIcon: React.FC<IconProps>;
export default SquareIcon;

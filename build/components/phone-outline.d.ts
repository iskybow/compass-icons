import React from 'react';
import IconProps from './props';
declare const PhoneOutlineIcon: React.FC<IconProps>;
export default PhoneOutlineIcon;

import React from 'react';
import IconProps from './props';
declare const ExclamationThickIcon: React.FC<IconProps>;
export default ExclamationThickIcon;

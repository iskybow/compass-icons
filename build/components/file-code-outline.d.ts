import React from 'react';
import IconProps from './props';
declare const FileCodeOutlineIcon: React.FC<IconProps>;
export default FileCodeOutlineIcon;

import React from 'react';
import IconProps from './props';
declare const FileImageOutlineLargeIcon: React.FC<IconProps>;
export default FileImageOutlineLargeIcon;

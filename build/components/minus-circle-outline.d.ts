import React from 'react';
import IconProps from './props';
declare const MinusCircleOutlineIcon: React.FC<IconProps>;
export default MinusCircleOutlineIcon;

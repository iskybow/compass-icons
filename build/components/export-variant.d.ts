import React from 'react';
import IconProps from './props';
declare const ExportVariantIcon: React.FC<IconProps>;
export default ExportVariantIcon;

import React from 'react';
import IconProps from './props';
declare const FileCodeOutlineLargeIcon: React.FC<IconProps>;
export default FileCodeOutlineLargeIcon;

import React from 'react';
import IconProps from './props';
declare const MessagePlusOutlineIcon: React.FC<IconProps>;
export default MessagePlusOutlineIcon;

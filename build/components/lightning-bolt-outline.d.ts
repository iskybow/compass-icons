import React from 'react';
import IconProps from './props';
declare const LightningBoltOutlineIcon: React.FC<IconProps>;
export default LightningBoltOutlineIcon;

import React from 'react';
import IconProps from './props';
declare const WebhookOutgoingIcon: React.FC<IconProps>;
export default WebhookOutgoingIcon;

import React from 'react';
import IconProps from './props';
declare const ArrowRightBoldOutlineIcon: React.FC<IconProps>;
export default ArrowRightBoldOutlineIcon;

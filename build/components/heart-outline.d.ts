import React from 'react';
import IconProps from './props';
declare const HeartOutlineIcon: React.FC<IconProps>;
export default HeartOutlineIcon;

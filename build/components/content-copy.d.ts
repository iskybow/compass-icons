import React from 'react';
import IconProps from './props';
declare const ContentCopyIcon: React.FC<IconProps>;
export default ContentCopyIcon;

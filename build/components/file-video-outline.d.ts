import React from 'react';
import IconProps from './props';
declare const FileVideoOutlineIcon: React.FC<IconProps>;
export default FileVideoOutlineIcon;

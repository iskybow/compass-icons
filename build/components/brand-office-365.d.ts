import React from 'react';
import IconProps from './props';
declare const BrandOffice365Icon: React.FC<IconProps>;
export default BrandOffice365Icon;

import React from 'react';
import IconProps from './props';
declare const FormatClearIcon: React.FC<IconProps>;
export default FormatClearIcon;

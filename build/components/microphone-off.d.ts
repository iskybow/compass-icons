import React from 'react';
import IconProps from './props';
declare const MicrophoneOffIcon: React.FC<IconProps>;
export default MicrophoneOffIcon;

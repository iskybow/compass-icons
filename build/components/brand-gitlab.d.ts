import React from 'react';
import IconProps from './props';
declare const BrandGitlabIcon: React.FC<IconProps>;
export default BrandGitlabIcon;

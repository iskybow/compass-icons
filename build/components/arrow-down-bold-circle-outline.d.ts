import React from 'react';
import IconProps from './props';
declare const ArrowDownBoldCircleOutlineIcon: React.FC<IconProps>;
export default ArrowDownBoldCircleOutlineIcon;

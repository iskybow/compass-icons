import React from 'react';
import IconProps from './props';
declare const GlobeIcon: React.FC<IconProps>;
export default GlobeIcon;

import React from 'react';
import IconProps from './props';
declare const CrownOutlineIcon: React.FC<IconProps>;
export default CrownOutlineIcon;

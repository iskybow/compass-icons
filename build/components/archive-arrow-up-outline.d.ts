import React from 'react';
import IconProps from './props';
declare const ArchiveArrowUpOutlineIcon: React.FC<IconProps>;
export default ArchiveArrowUpOutlineIcon;

import React from 'react';
import IconProps from './props';
declare const DownloadOutlineIcon: React.FC<IconProps>;
export default DownloadOutlineIcon;

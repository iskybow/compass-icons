import React from 'react';
import IconProps from './props';
declare const CheckCircleIcon: React.FC<IconProps>;
export default CheckCircleIcon;

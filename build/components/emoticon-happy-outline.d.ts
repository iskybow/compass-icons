import React from 'react';
import IconProps from './props';
declare const EmoticonHappyOutlineIcon: React.FC<IconProps>;
export default EmoticonHappyOutlineIcon;

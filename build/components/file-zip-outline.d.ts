import React from 'react';
import IconProps from './props';
declare const FileZipOutlineIcon: React.FC<IconProps>;
export default FileZipOutlineIcon;

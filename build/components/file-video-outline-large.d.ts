import React from 'react';
import IconProps from './props';
declare const FileVideoOutlineLargeIcon: React.FC<IconProps>;
export default FileVideoOutlineLargeIcon;

import React from 'react';
import IconProps from './props';
declare const AlertOutlineIcon: React.FC<IconProps>;
export default AlertOutlineIcon;

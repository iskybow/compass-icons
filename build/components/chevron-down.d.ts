import React from 'react';
import IconProps from './props';
declare const ChevronDownIcon: React.FC<IconProps>;
export default ChevronDownIcon;

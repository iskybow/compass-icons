import React from 'react';
import IconProps from './props';
declare const CircleMultipleOutlineIcon: React.FC<IconProps>;
export default CircleMultipleOutlineIcon;

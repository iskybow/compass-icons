import React from 'react';
import IconProps from './props';
declare const FileImageBrokenOutlineLargeIcon: React.FC<IconProps>;
export default FileImageBrokenOutlineLargeIcon;

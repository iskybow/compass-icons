import React from 'react';
import IconProps from './props';
declare const FileImageOutlineIcon: React.FC<IconProps>;
export default FileImageOutlineIcon;

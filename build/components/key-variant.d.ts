import React from 'react';
import IconProps from './props';
declare const KeyVariantIcon: React.FC<IconProps>;
export default KeyVariantIcon;

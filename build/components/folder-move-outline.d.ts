import React from 'react';
import IconProps from './props';
declare const FolderMoveOutlineIcon: React.FC<IconProps>;
export default FolderMoveOutlineIcon;

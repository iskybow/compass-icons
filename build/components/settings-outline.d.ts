import React from 'react';
import IconProps from './props';
declare const SettingsOutlineIcon: React.FC<IconProps>;
export default SettingsOutlineIcon;

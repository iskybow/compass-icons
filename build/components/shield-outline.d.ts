import React from 'react';
import IconProps from './props';
declare const ShieldOutlineIcon: React.FC<IconProps>;
export default ShieldOutlineIcon;

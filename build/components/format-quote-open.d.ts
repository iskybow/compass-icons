import React from 'react';
import IconProps from './props';
declare const FormatQuoteOpenIcon: React.FC<IconProps>;
export default FormatQuoteOpenIcon;

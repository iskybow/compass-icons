import React from 'react';
import IconProps from './props';
declare const FormatStrikethroughVariantIcon: React.FC<IconProps>;
export default FormatStrikethroughVariantIcon;

import React from 'react';
import IconProps from './props';
declare const CodeBracketsIcon: React.FC<IconProps>;
export default CodeBracketsIcon;

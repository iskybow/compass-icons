import React from 'react';
import IconProps from './props';
declare const StarIcon: React.FC<IconProps>;
export default StarIcon;

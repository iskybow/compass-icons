import React from 'react';
import IconProps from './props';
declare const FilePdfOutlineLargeIcon: React.FC<IconProps>;
export default FilePdfOutlineLargeIcon;

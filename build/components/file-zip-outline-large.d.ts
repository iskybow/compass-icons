import React from 'react';
import IconProps from './props';
declare const FileZipOutlineLargeIcon: React.FC<IconProps>;
export default FileZipOutlineLargeIcon;

"use strict";
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(require("react"));
const WorkchatIcon = (_a) => {
    var { size, color } = _a, rest = __rest(_a, ["size", "color"]);
    return (react_1.default.createElement("svg", Object.assign({ xmlns: "http://www.w3.org/2000/svg", version: "1.1", width: size || 24, height: size || 24, fill: color || '#000000', viewBox: "0 0 24 24" }, rest),
        react_1.default.createElement("g", { "clip-path": "url(#clip0_133_29)" },
            react_1.default.createElement("path", { "fill-rule": "evenodd", "clip-rule": "evenodd", d: "M27.0742 13C27.0577 14.3178 26.6867 15.6135 25.9916 16.7816H26.335C30.6657 16.7816 30.6657 19.4458 30.6657 20.5862C30.6657 21.5267 30.6657 24.3908 26.335 24.3908H21.982C20.6407 24.3932 19.3184 24.6696 18.1197 25.1979C16.921 25.7263 15.8787 26.4922 15.0754 27.4352C14.5339 27.0827 14.0942 26.6236 13.7914 26.0949C13.4886 25.5662 13.3313 24.9826 13.332 24.3908V20.5731C13.332 19.1444 13.836 18.2432 14.5416 17.6763C13.5183 17.387 12.4366 17.2921 11.3683 17.3977C11.0286 17.3977 10.6926 17.4239 10.3603 17.4239H9.61364C9.18852 18.4312 8.98089 19.4991 9.00138 20.5731V24.3908C9.02372 26.4017 9.94318 28.3248 11.5625 29.7473C13.1817 31.1699 15.3718 31.9787 17.6626 32C17.6692 31.3058 17.8936 30.6265 18.3111 30.0369C18.7287 29.4472 19.3232 28.97 20.0295 28.6575C21.982 27.9005 23.4753 28.1954 24.9687 28.1954H26.335C30.6657 28.1954 35 26.0555 35 20.5862C35 15.3299 31.1435 13.2196 27.0742 13Z", fill: "black" }),
            react_1.default.createElement("path", { "fill-rule": "evenodd", "clip-rule": "evenodd", d: "M8.66169 16.0138H10.0319C11.5253 16.0138 13.0187 15.7034 14.9675 16.5034C15.6708 16.8304 16.2634 17.3293 16.6808 17.9458C17.0982 18.5624 17.3243 19.2729 17.3346 20C19.6242 19.9803 21.8142 19.1321 23.4344 17.6377C25.0545 16.1432 25.9757 14.1216 26 12.0069V8.0069C26 2.47931 21.6542 0 17.3346 0H8.66169C4.33458 0 0 2.12759 0 8.0069C0 13.7621 4.33458 16.0138 8.66169 16.0138ZM8.66169 4.00345H17.3346C18.9586 4.00345 21.6542 4.52414 21.6542 8.0069V12.0103C21.6556 12.6326 21.4985 13.2465 21.1956 13.8024C20.8927 14.3582 20.4526 14.8405 19.9107 15.2103C19.105 14.2173 18.0601 13.4112 16.8587 12.8557C15.6573 12.3003 14.3323 12.0109 12.9888 12.0103H8.66169C4.33458 12.0103 4.33458 8.99655 4.33458 8.0069C4.33458 6.8069 4.33458 4.00345 8.66169 4.00345Z", fill: "black" })),
        react_1.default.createElement("defs", null,
            react_1.default.createElement("clipPath", { id: "clip0_133_29" },
                react_1.default.createElement("rect", { width: "35", height: "32", fill: "white" })))));
};
exports.default = WorkchatIcon;

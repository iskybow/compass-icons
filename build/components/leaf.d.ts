import React from 'react';
import IconProps from './props';
declare const LeafIcon: React.FC<IconProps>;
export default LeafIcon;

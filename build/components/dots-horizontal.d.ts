import React from 'react';
import IconProps from './props';
declare const DotsHorizontalIcon: React.FC<IconProps>;
export default DotsHorizontalIcon;

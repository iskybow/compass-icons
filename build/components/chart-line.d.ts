import React from 'react';
import IconProps from './props';
declare const ChartLineIcon: React.FC<IconProps>;
export default ChartLineIcon;

import React from 'react';
import IconProps from './props';
declare const FolderOutlineIcon: React.FC<IconProps>;
export default FolderOutlineIcon;

import React from 'react';
import IconProps from './props';
declare const SendOutlineIcon: React.FC<IconProps>;
export default SendOutlineIcon;

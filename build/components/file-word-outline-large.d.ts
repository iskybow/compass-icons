import React from 'react';
import IconProps from './props';
declare const FileWordOutlineLargeIcon: React.FC<IconProps>;
export default FileWordOutlineLargeIcon;

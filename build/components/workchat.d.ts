import React from 'react';
import IconProps from './props';
declare const WorkchatIcon: React.FC<IconProps>;
export default WorkchatIcon;

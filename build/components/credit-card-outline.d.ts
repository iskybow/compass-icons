import React from 'react';
import IconProps from './props';
declare const CreditCardOutlineIcon: React.FC<IconProps>;
export default CreditCardOutlineIcon;

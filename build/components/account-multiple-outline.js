"use strict";
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(require("react"));
const AccountMultipleOutlineIcon = (_a) => {
    var { size, color } = _a, rest = __rest(_a, ["size", "color"]);
    return (react_1.default.createElement("svg", Object.assign({ xmlns: "http://www.w3.org/2000/svg", version: "1.1", width: size || 24, height: size || 24, fill: color || '#000000', viewBox: "0 0 24 24" }, rest),
        react_1.default.createElement("path", { d: "M16.3,16.2l-2.5-1.5c1-1.4,1.6-3.3,1.6-5.2C15.4,6.2,13.2,4,10,4S4.6,6.2,4.6,9.5c0,1.9,0.6,3.8,1.6,5.2l-2.5,1.5\nc-1,0.5-1.7,1.6-1.7,2.7C2,20.6,3.4,22,5,22h10c1.7,0,3-1.4,3-3.1C18,17.8,17.3,16.7,16.3,16.2z M6.6,9.5C6.6,7.1,8.1,6,10,6\n\ts3.4,1.1,3.4,3.5c0,1.5-0.5,3.1-1.4,4.2c-0.5,0.7-1.2,1.1-2.1,1.1c-0.9,0-1.5-0.4-2.1-1.1C7.1,12.6,6.6,11,6.6,9.5z M15,20H5\n\tc-0.6,0-1-0.5-1-1.1c0-0.1,0-0.2,0-0.3c0.1-0.2,0.2-0.4,0.3-0.5c0.1,0,0.2-0.1,0.3-0.1l3.1-1.9c0.7,0.4,1.4,0.6,2.3,0.6\n\ts1.6-0.2,2.3-0.6l3.1,1.9c0.4,0.2,0.6,0.5,0.6,1S15.5,20,15,20z M18.9,13.1c0.3-1.2,0.5-2.4,0.5-3.6c0-0.4,0-3.1-1.9-5.5\n\tC16.7,3,15,2,14,2c-0.7,0-1.3,0.1-1.8,0.3c1.3,0.4,2.5,1.1,3.4,2c1,1.1,1.6,2.5,1.8,4.1c0,0.4,0.1,0.7,0.1,1.1\n\tc0,1.6-0.3,3.1-0.9,4.5l0.8,0.5c0.5,0.3,1,0.7,1.4,1.1c0.5,0.6,0.9,1.2,1.1,2c0.1,0.5,0.2,0.9,0.2,1.4c0,0.3,0,0.6-0.1,0.9\n\tc1-0.3,1.7-1.1,2-2.1c0.1-0.3,0.1-0.5,0.1-0.8C22,15.8,21.1,14,18.9,13.1z" })));
};
exports.default = AccountMultipleOutlineIcon;

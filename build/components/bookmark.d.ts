import React from 'react';
import IconProps from './props';
declare const BookmarkIcon: React.FC<IconProps>;
export default BookmarkIcon;

import React from 'react';
import IconProps from './props';
declare const EyeOutlineIcon: React.FC<IconProps>;
export default EyeOutlineIcon;

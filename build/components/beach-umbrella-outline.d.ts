import React from 'react';
import IconProps from './props';
declare const BeachUmbrellaOutlineIcon: React.FC<IconProps>;
export default BeachUmbrellaOutlineIcon;

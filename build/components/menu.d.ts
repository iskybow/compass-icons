import React from 'react';
import IconProps from './props';
declare const MenuIcon: React.FC<IconProps>;
export default MenuIcon;

import React from 'react';
import IconProps from './props';
declare const CameraOutlineIcon: React.FC<IconProps>;
export default CameraOutlineIcon;

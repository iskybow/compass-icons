import React from 'react';
import IconProps from './props';
declare const MenuLeftIcon: React.FC<IconProps>;
export default MenuLeftIcon;

import React from 'react';
import IconProps from './props';
declare const AirplaneIcon: React.FC<IconProps>;
export default AirplaneIcon;

import React from 'react';
import IconProps from './props';
declare const ChevronUpIcon: React.FC<IconProps>;
export default ChevronUpIcon;

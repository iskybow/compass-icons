import React from 'react';
import IconProps from './props';
declare const AccountOutlineIcon: React.FC<IconProps>;
export default AccountOutlineIcon;

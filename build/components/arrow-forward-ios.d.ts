import React from 'react';
import IconProps from './props';
declare const ArrowForwardIosIcon: React.FC<IconProps>;
export default ArrowForwardIosIcon;

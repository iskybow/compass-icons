import React from 'react';
import IconProps from './props';
declare const ThumbsUpDownIcon: React.FC<IconProps>;
export default ThumbsUpDownIcon;

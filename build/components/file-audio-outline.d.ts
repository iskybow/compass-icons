import React from 'react';
import IconProps from './props';
declare const FileAudioOutlineIcon: React.FC<IconProps>;
export default FileAudioOutlineIcon;

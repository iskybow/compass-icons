import React from 'react';
import IconProps from './props';
declare const HelpCircleOutlineIcon: React.FC<IconProps>;
export default HelpCircleOutlineIcon;
